from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .encoders import SalespersonEncoder, CustomerEncoder, SaleEncoder
from .models import Salesperson, Customer, Sale, AutomobileVO
from django.http import JsonResponse
import json


#  SALESPERSON

@require_http_methods(["GET", "POST"])
def api_list_salespersons(request):
    if request.method == "GET":
        salespersons = Salesperson.objects.all()
        return JsonResponse(
            {"salespersons": salespersons},
            encoder=SalespersonEncoder,
            safe=False,
        )

    else:  # POST request. Create Salesperson
        content = json.loads(request.body)

        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_salesperson(request, pk):
    if request.method == "GET":
        salesperson = Salesperson.objects.get(id=pk)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            count, _ = Salesperson.objects.get(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson does not exist."},
                status=400,
            )
    else:  # PUT request (Update salesperson)
        content = json.loads(request.body)
        try:
            salesperson = Salesperson.objects.get(id=pk)
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson does not exist."},
                status=400,
            )
        Salesperson.objects.filter(id=pk).update(**content)

        salesperson = Salesperson.objects.get(id=pk)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False
        )

#  CUSTOMER
@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
            safe=False,
        )

    else:  # POST request. Create customer
        content = json.loads(request.body)

        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_customer(request, pk):
    if request.method == "GET":
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            count, _ = Customer.objects.get(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist."},
                status=400,
            )
    else:  # PUT request (Update customer)
        content = json.loads(request.body)
        try:
            customer = Customer.objects.get(id=pk)
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist."},
                status=400,
            )
        Customer.objects.filter(id=pk).update(**content)

        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False
        )


#  SALE
@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
            safe=False,
        )

    else:  # POST request. Create sale
        content = json.loads(request.body)
        print("Content: ", content)
        try:
            automobile_vin = content["vin"]
            auto_vin = AutomobileVO.objects.get(vin=automobile_vin)
            print("Auto VIN: ", auto_vin)
            content["vin"] = auto_vin
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid VIN"},
                status=400,
            )

        try:
            customer_id = content["customer_id"]
            customer = Customer.objects.get(id=customer_id)
            content["customer_id"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer ID"},
                status=400,
            )

        try:
            employee_id = content["employee_id"]
            employee = Salesperson.objects.get(employee_id=employee_id)
            content["employee_id"] = employee
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid employee ID"},
                status=400,
            )

        sale = Sale.objects.create(
            automobile=content["vin"],
            salesperson=content["employee_id"],
            customer=content["customer_id"],
            price=content["price"],
        )
        print("Sale: ", sale)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )

# SHOW SALE
@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_sale(request, pk):
    if request.method == "GET":
        sale = Sale.objects.get(id=pk)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            count, _ = Sale.objects.get(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Sale does not exist."},
                status=400,
            )
    else:  # PUT request (Update customer)
        content = json.loads(request.body)
        try:
            sale = Sale.objects.get(id=pk)
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Sale does not exist."},
                status=400,
            )
        Sale.objects.filter(id=pk).update(**content)

        sale = Sale.objects.get(id=pk)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False
        )

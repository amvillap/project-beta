# Generated by Django 4.0.3 on 2023-12-20 02:33

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0002_salesperson_automobile'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='salesperson',
            name='automobile',
        ),
    ]

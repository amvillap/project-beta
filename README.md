# CarCar
Jack-of-all trades in auto! Find everything related to auto here!
CarCar is an application that contains and manages the following:
 - Manufacturers
 - Vehicle Models
 - Automobiles
 - Technicians/Salespeople
 - Customers/Appointments
 - Salesperson/Service History

## Team:
* Angelika Villapando - Service microservice
* Justin Huh - Sales microservice


## How to Run this App

1) Fork the repository from here: https://gitlab.com/amvillap/project-beta
2) Clone the repository you forked and into the directory you want it to clone to:
	- On the website provided, click the blue button "Code", and copy the url in "Clone with HTTPS".
	- In terminal, change directory (cd) to your desired directory.
	- clone it via this command: git clone <repository.url> (without the <>)
3) In Docker, run these commands:
	- docker volume create beta-data
 	- docker-compose build
 	- docker-compose up

	- Once all the containers are running, view the project in: http://localhost:3000/
	- It should be initially directed to the homepage.


## How to Run this App

* Ensure that you have Docker and Git before proceeding.

1) Fork the repository from here: https://gitlab.com/amvillap/project-beta
2) Clone the repository you forked and into the directory you want it to clone to:
    - On the website provided, click the blue button "Code", and copy the url in "Clone with HTTPS".
    - In terminal, change directory (cd) to your desired directory.
    - clone it via this command: git clone <repository.url> (without the <>)
3) In Docker, run these commands:
    - docker volume create beta-data
    - docker-compose build
    - docker-compose up
    - Once all the containers are running, view the project in: http://localhost:3000/
    - It should be initially directed to the homepage.

## Diagram
Located in: /ghi/app/public/CarCarDiagram.png

## URLs and Ports
Main Page: http://localhost:3000/
Manufacturers (list): http://localhost:3000/manufacturers
Manufacturer (create): http://localhost:3000/manufacturers/create
Vehicle Models (list): http://localhost:3000/models
Vehicle Model (create): http://localhost:3000/vehicles/create
Automobiles (list): http://localhost:3000/automobiles
Automobile (create): http://localhost:3000/automobiles/create
Technicians (list): http://localhost:3000/technicians
Technicians (create): http://localhost:3000/technicians/create
Salespeople (list): http://localhost:3000/salespeople
Salesperson (create): http://localhost:3000/salespeople/create
Customers (list): http://localhost:3000/customers
Customer (create): http://localhost:3000/customers/create
Appointments (list): http://localhost:3000/appointments
Appointments (create): http://localhost:3000/appointments/create
Sales (list): http://localhost:3000/sales
Sales (create): http://localhost:3000/sales/create
Service History: http://localhost:3000/servicehistory
Sale History: http://localhost:3000/salespeople/history


## SERVICE MICROSERVICE: Angelika Villapando

The service microservice keeps track of the services performed at our dealership. Specifically, it keeps track of our technicians, the automobiles being serviced (whether or not it's bought from our dealership or not), and appointments. Like the sales microservice, the service microservice communicates with the inventory microservice via a *poller*. The poller sends data for the automobiles in our inventory. This is primarily used to keep track of which customers getting serviced have purchased their vehicle from our dealership, via their vehicle's VIN number, making them a *VIP* customer. The following further explains the models utilized for this microservice.

* IDs
When instances of each model is created an ID is automatically created and associated with each instance. This is utilized when referring to a specific instance of a model as demonstrated below.

* Insomnia
Each request method is followed by example input (JSON Body) and return values to demonstrate the data types/structure accepted/returned by these requests.

* Formatting:

```MODEL```
Action -> Request Method -> URL

INPUT/RETURN VALUES


```TECHNICIAN```
List all techncians -> GET -> http://localhost:8080/api/technicians/

SHOULD RETURN:
{
	"technicians": [
		{
			"href": "/api/technicians/2/",
			"id": 2,
			"first_name": "Sean",
			"last_name": "Young",
			"employee_id": "swyoung"
		}...
    ]
}

View individual technician -> GET -> http://localhost:8080/api/technicians/6

SHOULD RETURN:
* id is automatically designated, while the employee_id is assigned by the user

{
	"href": "/api/technicians/6/",
	"id": 6,
	"first_name": "Ricky",
	"last_name": "Azul",
	"employee_id": "razul"
}

Register/create a technician -> POST -> http://localhost:8080/api/technicians/create

SEND:
{
	"first_name": "Ricky",
	"last_name": "Azul",
	"employee_id": "razul"
}

SHOULD RETURN:
{
	"href": "/api/technicians/6/",
	"id": 6,
	"first_name": "Ricky",
	"last_name": "Azul",
	"employee_id": "razul"
}

Delete a specific technician -> DELETE -> http://localhost:8080/api/technicians/6

SHOULD RETURN (if technician exists):
{
	"message": "Technician deleted."
}

SHOULD RETURN(if technician does not exist):
{
	"message": "That technician does not exist."
}

```AUTOMOBILE VO```
* Data for the Automobile VO (value object) model is obtained from polling the inventory microservice.

List all automobiles -> GET -> 	http://localhost:8100/api/automobiles/

SHOULD RETURN:
{
	"autos": [
		{
			"href": "/api/automobiles/1GNEK13Z73R302960/",
			"id": 1,
			"color": "silver",
			"year": 2013,
			"vin": "1GNEK13Z73R302960",
			"model": {
				"href": "/api/models/3/",
				"id": 3,
				"name": "Prius",
				"picture_url": "https://global.toyota/pages/news/images/2022/11/16/1345/001.jpg",
				"manufacturer": {
					"href": "/api/manufacturers/4/",
					"id": 4,
					"name": "Toyota"
				}
			},
			"sold": false
		}
    ]
}

Create automobiles -> POST -> http://localhost:8100/api/automobiles/create
SEND:
{
  "color": "Yellow",
  "year": 2026,
  "vin": "1G8ZR127X1Z271796",
  "model_id": 8
}

```APPOINTMENT```
List all appointment -> GET -> http://localhost:8080/api/appointments/
SHOULD RETURN:
{
	"appointments": [
		{
			"href": "/api/appointments/8/",
			"vin": null,
			"id": 8,
			"date_time": "2024-01-24T00:00:00+00:00",
			"reason": "I need an oil change",
			"status": "canceled",
			"customer": "Sean",
			"technician": {
				"href": "/api/technicians/3/",
				"id": 3,
				"first_name": "Angelika",
				"last_name": "Villapando",
				"employee_id": "amvillap"
			}
		}
    ]
}

Show individual appointment -> GET -> http://localhost:8080/api/appointments/8
SHOULD RETURN:
    {
    "href": "/api/appointments/8/",
    "vin": null,
    "id": 8,
    "date_time": "2024-01-24T00:00:00+00:00",
    "reason": "I need an oil change",
    "status": "canceled",
    "customer": "Sean",
    "technician": {
        "href": "/api/technicians/3/",
        "id": 3,
        "first_name": "Angelika",
        "last_name": "Villapando",
        "employee_id": "amvillap"
    }

Create an appointment -> POST -> http://localhost:8080/api/appointments/create
SEND:
{
	"vin": "4F2YZ041X4KM03443",
	"reason": "Paint windshield wipers",
	"date_time": "2024-10-30 10:30",
	"customer": "Layla",
	"technician_id": 2
}

SHOULD RETURN:
{
	"href": "/api/appointments/36/",
	"vin": "4F2YZ041X4KM03443",
	"id": 36,
	"date_time": "2024-10-30 10:30",
	"reason": "Paint windshield wipers",
	"status": "select status",
	"customer": "Layla",
	"technician": {
		"href": "/api/technicians/2/",
		"id": 2,
		"first_name": "Sean",
		"last_name": "Young",
		"employee_id": "swyoung"
	}
}

Delete an appointment -> DELETE -> http://localhost:8080/api/appointments/8
SHOULD RETURN (if appointment exists):
{
	"message": "Appointment deleted"
}
SHOULD RETURN (if appointment does not exist):
{
	"message": "Does not exist"
}

Cancel an appointment -> PUT -> http://localhost:8080/api/appointments/29/cancel/
SHOULD RETURN: status updated
{
	"href": "/api/appointments/29/",
	"id": 29,
	"date_time": "2024-05-30T00:00:00+00:00",
	"reason": "Paint car red",
	"status": "canceled",
	"customer": "Edith",
	"technician": {
		"href": "/api/technicians/4/",
		"id": 4,
		"first_name": "Edith",
		"last_name": "Palacio",
		"employee_id": "evpalacio"
	}
}

Finish an appointment -> PUT -> http://localhost:8080/api/appointments/29/finish/
{
	"href": "/api/appointments/29/",
	"id": 29,
	"date_time": "2024-05-30T00:00:00+00:00",
	"reason": "Paint car red",
	"status": "canceled",
	"customer": "Edith",
	"technician": {
		"href": "/api/technicians/4/",
		"id": 4,
		"first_name": "Edith",
		"last_name": "Palacio",
		"employee_id": "evpalacio"
	}
}

## Sales microservice: Justin Huh ##
# Endpoints + (View/Create/Update/Delete Data) via Insomnia & Browser
```Manufacturers```
GET | List all manufacturers: http://localhost:8100/api/manufacturers/
GET | Single manufacturer: http://localhost:8100/api/manufacturers/id/
POST | Create manufacturer: http://localhost:8100/api/manufacturers/
PUT | Update manufacturer: http://localhost:8100/api/manufacturers/id/
DELETE | Delete manufacturer: http://localhost:8100/api/manufacturers/id/
`Insomnia` [Manufacturer]
`Input value`
Insert the format below to create/update a manufacturer.
{
    "name": "Test"
}
`Return value:`
{
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Test"
}
For deleting and viewing a single manufacturer, type in the URL with the specified id number
of the manufacturer. Click "Send" to view/delete. To view all manufacturers, type in the URL
provided.
````````````````````````````````````````````````break````````````````````````````````````````````````
```Vehicle Models```
GET | List all models: http://localhost:8100/api/models/
GET | Single model: http://localhost:8100/api/models/id/
POST | Create model: http://localhost:8100/api/models/
PUT | Update model: http://localhost:8100/api/models/id/
DELETE | Delete model: http://localhost:8100/api/models/id/
`Insomnia` [Model]
`Input value`
Insert the format below to create/update a manufacturer.
Name of model, picture URL, and manufacturer's ID number.
{
  "name": "7 Series",
  "picture_url": "https://www.topgear.com/sites/default/files/2023/08/P90492179_highRes_bmw-i7-xdrive60-m-sp%20%281%29.jpg",
  "manufacturer_id": 5
}
`Return value:`
{
    "href": "/api/models/7/",
    "id": 7,
    "name": "7 Series",
    "picture_url": "https://www.topgear.com/sites/default/files/2023/08/P90492179_highRes_bmw-i7-xdrive60-m-sp%20%281%29.jpg",
    "manufacturer": {
        "href": "/api/manufacturers/5/",
        "id": 5,
        "name": "BMW"
    }
}
For deleting and viewing a single vehicle model, type in the URL with the specified id number
of the vehicle model. Click "Send" to view/delete. To view all vehicle models, type in the URL
provided.
````````````````````````````````````````````````break````````````````````````````````````````````````
```Automobiles```
GET | List all automobiles: http://localhost:8100/api/automobiles/
GET | Single automobile: http://localhost:8100/api/automobiles/VIN#/
POST | Create automobile: http://localhost:8100/api/automobiles/
PUT | Update automobile: http://localhost:8100/api/automobiles/VIN#/
DELETE | Delete automobile: http://localhost:8100/api/automobiles/VIN#/
`Insomnia` [Automobiles]
`Input value`
Insert the format below to create/update an automobile.
***NOTE: VIN number is used to view/update/delete an automobile.***
{
    "color": "black",
    "year": 2024,
    "vin": "44440291AD21D1",
    "model_id": 7,
    "sold": true
}
`Return value:`
{
    "href": "/api/automobiles/44440291AD21D1/",
    "id": 17,
    "color": "black",
    "year": 2024,
    "vin": "44440291AD21D1",
    "model": {
        "href": "/api/models/7/",
        "id": 7,
        "name": "7 Series",
        "picture_url": "https://www.topgear.com/sites/default/files/2023/08/P90492179_highRes_bmw-i7-xdrive60-m-sp%20%281%29.jpg",
        "manufacturer": {
            "href": "/api/manufacturers/5/",
            "id": 5,
            "name": "BMW"
        }
    },
    "sold": true
}
For deleting and viewing a single automobile, type in the URL with the specified VIN number
of the automobile. Click "Send" to view/delete. To view all manufacturers, type in the URL
provided.
````````````````````````````````````````````````break````````````````````````````````````````````````
```Customers```
GET | List all customers: http://localhost:8090/api/customers/
GET | Single customer: http://localhost:8090/api/customers/id/
POST | Create customer: http://localhost:8090/api/customers/
PUT | Update customer: http://localhost:8090/api/customers/id/
DELETE | Delete customer: http://localhost:8090/api/customers/id/
`Insomnia` [Customer]
`Input value`
Insert the format below to create/update a customer.
First and last name, address, and phone number of customer.
{
    "first_name": "Daniel",
    "last_name": "Kim",
    "address": "3512 Kirkland Ave., Los Angeles, CA 90203",
    "phone_number": "213-512-6931"
}
`Return value:`
{
    "href": "/api/customers/1/",
    "first_name": "Daniel",
    "last_name": "Kim",
    "address": "3512 Kirkland Ave., Los Angeles, CA 90203",
    "phone_number": "213-512-6931",
    "id": 1
}
For deleting and viewing a single customer, type in the URL with the specified id number
of the customer. Click "Send" to view/delete. To view all manufacturers, type in the URL
provided.
````````````````````````````````````````````````break````````````````````````````````````````````````
```Salespeople```
GET | List all salespeople: http://localhost:8090/api/salespeople/
GET | Single salesperson: http://localhost:8090/api/salespeople/id/
POST | Create salesperson: http://localhost:8090/api/salespeople/
PUT | Update salesperson: http://localhost:8090/api/salespeople/id/
DELETE | Delete salesperson: http://localhost:8090/api/salespeople/id/
`Insomnia` [Salespeople]
`Input value`
Insert the format below to create/update a Salesperson.
First and last name of employee, and employee ID number.
{
    "first_name": "John",
    "last_name": "Doe",
    "employee_id": 12345
}
`Return value:`
{
    "href": "/api/salespeople/1/",
    "first_name": "John",
    "last_name": "Doe",
    "employee_id": 12345,
    "id": 1
}
For deleting and viewing a single salesperson, type in the URL with the specified id number
of the salesperson. Click "Send" to view/delete. To view all salespeople, type in the URL
provided.
````````````````````````````````````````````````break````````````````````````````````````````````````
```Sales```
GET | List all sales: http://localhost:8090/api/sales/
GET | Single sale: http://localhost:8090/api/sales/id/
POST | Create sale: http://localhost:8090/api/sales/
PUT | Update sale: http://localhost:8090/api/sales/id/
DELETE | Delete sale: http://localhost:8090/api/sales/id/
`Insomnia` [Sales]
`Input value`
Insert the format below to create/update a sale.
Price of vehicle, VIN number of automobile sold, employee ID of salesperson, and customer ID.
{
    "price": 50000,
    "vin": "1C3CC5FB2AN123174",
    "employee_id": 53165,
    "customer_id": 3
}
`Return value:`
{
    "price": 50000,
    "automobile": {
        "vin": "1C3CC5FB2AN123174",
        "sold": true
    },
    "salesperson": {
        "href": "/api/salespeople/1/",
        "first_name": "John",
        "last_name": "Doe",
        "employee_id": 53165,
        "id": 1
    },
    "customer": {
        "href": "/api/customers/1/",
        "first_name": "Jane",
        "last_name": "Doe",
        "address": "3512 Kirkland Ave., Los Angeles, CA 90203",
        "phone_number": "213-512-6931",
        "id": 1
    },
    "id": 1
}
For deleting and viewing a single sale, type in the URL with the specified id number
of the sale. Click "Send" to view/delete. To view all sales, type in the URL
provided.

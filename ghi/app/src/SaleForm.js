import React, { useState, useEffect } from 'react';


function SaleForm() {
    const [vins, setVins] = useState([])
    const [salesPeople, setSalesPeople] = useState([])
    const [customers, setCustomers] = useState([])
    const [formData, setFormData] = useState({
        price: '',
        vin: '',
        employee_id: '',
        customer_id: '',
    })

    const [hasCreated, setHasCreated] = useState(false)

    // VIN Data
    const vinData = async () => {
        const url = 'http://localhost:8100/api/automobiles/'
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            const unsoldOnly = data.autos.filter(auto => !auto.sold)
            const unsoldVinOnly = unsoldOnly.map(auto => auto.vin)
            setVins(unsoldVinOnly)
        }
    }

    useEffect(() => {
        vinData()
    }, [])

    // salesPeopleData
    const salesPeopleData = async () => {
        const url = 'http://localhost:8090/api/salespeople/'
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            // return object with full_name & employee_id
            const salesPeopleInfo = data.salespersons.map(salesperson => ({
                "full_name": `${salesperson.first_name} ${salesperson.last_name}`,
                "employee_id": salesperson.employee_id
            }))
            setSalesPeople(salesPeopleInfo)
            // test info
        }
    }

    useEffect(() => {
        salesPeopleData()
    }, [])

    // customerData
    const customersData = async () => {
        const url = 'http://localhost:8090/api/customers/'
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            // return customer's full_name & customer_id
            const customerInfo = data.customers.map(customer => ({
                "full_name": `${customer.first_name} ${customer.last_name}`,
                "customer_id": customer.id
            }))
            setCustomers(customerInfo)

        }
    }

    useEffect(() => {
        customersData()
    }, [])

    // Submit Sale Form
    const handleSubmit = async (event) => {
        event.preventDefault()

        const url = 'http://localhost:8090/api/sales/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(url, fetchConfig)

        if (response.ok) {
            setFormData({
                price: '',
                vin: '',
                employee_id: '',
                customer_id: '',
            })

            setHasCreated(true)
        }
    }

    const handleChangeName = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        })
    }

    const formClasses = (!hasCreated) ? '' : 'd-none';
    const messageClasses = (!hasCreated) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';


    return (
        <div className="my-5">
            <div className="row">
                <div className="col col-sm-auto">
                    <img
                        width="300"
                        className="bg-white rounded shadow d-block mx-auto mb-4"
                        src="/Automobile_stub.svg.png"
                        alt='Car logo'
                    />
                </div>
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                            <form className={formClasses} onSubmit={handleSubmit} id="record-sale-form">
                                <h1 className="card-title">Record Sale</h1>
                                {/* AUTOMOBILE VIN */}
                                <p className="mb-3">
                                    Automobile VIN
                                </p>
                                <div className="mb-3">
                                    <select onChange={handleChangeName} name="vin" id="vin" required>
                                        <option value="">Choose an automobile VIN</option>
                                        {
                                            vins.map(vin => {
                                                return (
                                                    <option key={vin} value={vin}>{vin}</option>
                                                )
                                            })
                                        }
                                    </select>
                                </div>
                                {/* EMPLOYEE NAME */}
                                <p className="mb-3">
                                    Employee Name
                                </p>
                                <div className="mb-3">
                                    <select onChange={handleChangeName} name="employee_id" id="employee_id" required>
                                        <option value="">Select Employee</option>
                                        {
                                            salesPeople.map(salesperson => {
                                                return (
                                                    <option key={salesperson.employee_id} value={salesperson.employee_id}>{salesperson.full_name}</option>
                                                )
                                            })
                                        }
                                    </select>
                                </div>
                                {/* CUSTOMER NAME */}
                                <p className="mb-3">
                                    Customer Name
                                </p>
                                <div className="mb-3">
                                    <select onChange={handleChangeName} name="customer_id" id="customer_id" required>
                                        <option value="">Select Customer</option>
                                        {
                                            customers.map(customer => {
                                                return (
                                                    <option key={customer.customer_id} value={customer.customer_id}>{customer.full_name}</option>
                                                )
                                            })
                                        }
                                    </select>
                                </div>
                                {/* PRICE */}
                                <div className="row">
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleChangeName} required placeholder="Price" type="number" id="price" name="price" className="form-control" value={formData.price} />
                                            <label htmlFor="style_name">Price</label>
                                        </div>
                                    </div>
                                </div>
                                <button className="btn btn-lg btn-primary">Create</button>
                            </form>
                            <div className={messageClasses} id="success-message">
                                Recorded Sale!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );


}


export default SaleForm

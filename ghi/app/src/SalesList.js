import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';


function SalesList() {
    const [sales, setSales] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/sales/');

        if (response.ok) {
            const data = await response.json();
            setSales(data.sales)
        };
    };

    useEffect(() => {
        getData()
    }, [])

    return (
        <>
            <div className="mb-3 mt-3 d-grid gap-2 d-md-flex justify-content-md-end">
                <Link to="/sales/create" className="btn btn-primary btn-lg px-4 gap-3">
                    Record Sales
                </Link>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson Employee ID</th>
                        <th>Salesperson Name</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map(sale => {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.salesperson.employee_id}</td>
                                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                                <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>{sale.price}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table >
        </>
    )


}



export default SalesList

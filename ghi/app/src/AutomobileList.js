import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function AutomobileList() {
    const [automobiles, setAutomobiles] = useState([])

    const getData = async () => {
        const response = await fetch('	http://localhost:8100/api/automobiles/');

        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos)
        };
    };

    useEffect(() => {
        getData()
    }, [])


    return (
        <>
            <div className="mb-3 mt-3 d-grid gap-2 d-md-flex justify-content-md-end">
                <Link to="/automobiles/create" className="btn btn-primary btn-lg px-4 gap-3">
                    Create Automobile
                </Link>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Model</th>
                        <th>Year</th>
                        <th>Color</th>
                        <th>Sold</th>
                    </tr>
                </thead>
                <tbody>
                    {automobiles.map(automobile => {
                        return (
                            <tr key={automobile.vin}>
                                <td>{automobile.vin}</td>
                                <td>{automobile.model.name}</td>
                                <td>{automobile.year}</td>
                                <td>{automobile.color}</td>
                                {/* condition ? expreIfTrue : exprIfFalse */}
                                <td>{automobile.sold ? 'Yes' : 'No'}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table >
        </>
    )


}


export default AutomobileList

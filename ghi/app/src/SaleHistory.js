import { useEffect, useState } from 'react';


function SaleHistory() {
    const [salesPeople, setSalesPeople] = useState([])
    const [query, setQuery] = useState('')

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/sales/')

        if (response.ok) {
            const data = await response.json()
            const soldOnly = data.sales.filter(sold => sold.automobile.sold === true)
            setSalesPeople(soldOnly)
        }

    }

    useEffect(() => {
        getData()
    }, [])

    return (
        <>
            <div className="row">
                <div className="offset-0 col-15">
                    <div className="shadow p-4 mt-4">
                        <h1>Salesperson History</h1>
                        <div className="mb-3">
                            <select onChange={(e) => setQuery(e.target.value)} name="salesPeople" id="salesPeople" required>
                                <option value="">Select Salesperson</option>
                                {salesPeople.map(salesperson => {
                                    return (
                                        <option key={salesperson.salesperson.employee_id} value={salesperson.salesperson.employee_id}>{salesperson.salesperson.first_name} {salesperson.salesperson.last_name}</option>
                                    )
                                })
                                }
                            </select>
                        </div>

                        <table className="table table-striped">
                            <thead>
                                <tr>
                                    <th>Salesperson</th>
                                    <th>Customer</th>
                                    <th>VIN</th>
                                    <th>Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                {/* Filter out employee ID to match query [query=e.target.value from dropdown]; convert query to number */}
                                {salesPeople.filter((salesperson) => salesperson.salesperson.employee_id === +query).map((info) => (
                                    <tr key={info.salesperson.employee_id}>
                                        <td>{info.salesperson.first_name} {info.salesperson.last_name}</td>
                                        <td>{info.customer.first_name} {info.customer.last_name}</td>
                                        <td>{info.automobile.vin}</td>
                                        <td>{info.price}</td>
                                    </tr>
                                ))}
                            </tbody>
                        </table >
                    </div>
                </div>
            </div>
        </>
    )
}

export default SaleHistory

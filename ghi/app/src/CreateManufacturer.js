import React, { useState, useEffect } from 'react';

function ManufacturerForm() {
    const [formData, setFormData] = useState({ name: '' });
    const [hasCreated, setHasCreated] = useState(false)

    const handleSubmit = async (event) => {
        event.preventDefault();

        const locationUrl = 'http://localhost:8100/api/manufacturers/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                name: '',
            });
            setHasCreated(true);
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({ ...formData, [inputName]: value });
    }

    const formClasses = (!hasCreated) ? '' : 'd-none';
    const messageClasses = (!hasCreated) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';



    return (
        <>
            <div className="card position-absolute top-50 start-50 translate-middle">
                <div className="card-body">
                    <form className={formClasses} onSubmit={handleSubmit}>
                        <label htmlFor="name" className="text-center">Add a Manufacturer</label>
                        <div className="col-sm-10">
                            <input
                                onChange={handleFormChange}
                                value={formData.name}
                                type="text"
                                className="form-control"
                                id="name"
                                placeholder="name"
                                name="name"
                            />
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-10">
                                <button type="submit" className="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>

                    <div className={messageClasses} id="success-message">
                        Created Manufacturer!
                    </div>
                </div>
            </div>
        </>
    )
}

export default ManufacturerForm;

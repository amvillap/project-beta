import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';


function SalespeopleList() {
    const [salespeople, setSalespeople] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');

        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespersons)
        };
    };

    useEffect(() => {
        getData()
    }, [])


    return (
        <>
            <div className="mb-3 mt-3 d-grid gap-2 d-md-flex justify-content-md-end">
                <Link to="/salespeople/create" className="btn btn-primary btn-lg px-4 gap-3">
                    Create Salesperson
                </Link>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Employee ID</th>
                    </tr>
                </thead>
                <tbody>
                    {salespeople.map(salesperson => {
                        return (
                            <tr key={salesperson.id}>
                                <td>{salesperson.first_name}</td>
                                <td>{salesperson.last_name}</td>
                                <td>{salesperson.employee_id}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table >
        </>
    )
}



export default SalespeopleList

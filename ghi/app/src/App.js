import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';
import VehicleForm from './VehicleForm';
import ListManufacturers from './ManufacturerList';
import ManufacturerForm from './CreateManufacturer';
import ModelList from './ModelList';
import SalespeopleList from './SalespeopleList';
import SalespeopleForm from './SalespeopleForm';
import CustomersList from './CustomersList';
import CustomerForm from './CustomerForm';
import TechnicianList from './TechnicianList';
import TechnicianForm from './TechnicianForm';
import SalesList from './SalesList';
import SaleForm from './SaleForm';
import SaleHistory from './SaleHistory';
import AppointmentList from './AppointmentList';
import ServiceHistory from './ServiceHistory';
import CreateAppointment from './CreateAppointment';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/automobiles" element={<AutomobileList />} />
          <Route path="/automobiles/create" element={<AutomobileForm />} />
          <Route path="/vehicles/create" element={<VehicleForm />} />
          <Route path="/manufacturers" element={<ListManufacturers />} />
          <Route path="/manufacturers/create" element={<ManufacturerForm />} />
          <Route path="/models" element={<ModelList />} />
          <Route path="/technicians" element={<TechnicianList />} />
          <Route path="/technicians/create" element={<TechnicianForm />} />
          <Route path="/salespeople" element={<SalespeopleList />} />
          <Route path="/salespeople/create" element={<SalespeopleForm />} />
          <Route path="/customers" element={<CustomersList />} />
          <Route path="/customers/create" element={<CustomerForm />} />
          <Route path="/sales" element={<SalesList />} />
          <Route path="/sales/create" element={<SaleForm />} />
          <Route path="/salespeople/history" element={<SaleHistory />} />
          <Route path="/appointments" element={<AppointmentList />} />
          <Route path="/appointments/create" element={<CreateAppointment />} />
          <Route path="/servicehistory" element={<ServiceHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

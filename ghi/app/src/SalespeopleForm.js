import React, { useState } from 'react';


function SalespeopleForm() {
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        employee_id: '',
    })

    const [hasCreated, setHasCreated] = useState(false)

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = "http://localhost:8090/api/salespeople/";

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                first_name: '',
                last_name: '',
                employee_id: '',
            })

            setHasCreated(true);
        };
    }

    const handleChangeName = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    const formClasses = (!hasCreated) ? '' : 'd-none';
    const messageClasses = (!hasCreated) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';


    return (
        <div className="my-5">
            <div className="row">
                <div className="col col-sm-auto">
                    <img
                        width="300"
                        className="bg-white rounded shadow d-block mx-auto mb-4"
                        src="/salesperson.jpg"
                        alt='Salesperson Logo'
                    />
                </div>

                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">

                            <form className={formClasses} onSubmit={handleSubmit} id="create-salespeople-form">
                                <p className="mb-3">
                                    Input salesperson details.
                                </p>

                                <div className="row">
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleChangeName} required placeholder="First Name" type="text" id="first_name" name="first_name" className="form-control" value={formData.first_name} />
                                            <label htmlFor="color">First Name</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleChangeName} required placeholder="Last Name" type="text" id="last_name" name="last_name" className="form-control" value={formData.last_name} />
                                            <label htmlFor="fabric">Last Name</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleChangeName} required placeholder="Employee ID" type="number" id="employee_id" name="employee_id" className="form-control" value={formData.employee_id} />
                                            <label htmlFor="style_name">Employee ID</label>
                                        </div>
                                    </div>

                                </div>
                                <button className="btn btn-lg btn-primary">Create</button>
                            </form>

                            <div className={messageClasses} id="success-message">
                                Created Salesperson!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default SalespeopleForm

import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function ModelColumn(props) {
    return (
        <div className="col">
            {props.list.map(data => {
                const model = data;
                return (
                    <div key={model.href} className="card mb-3 shadow">
                        <img
                            src={model.picture_url}
                            className="card-img-top"
                            alt='car model'
                        />
                        <div className="card-body">
                            <h5 className="card-title text-center">{model.name}</h5>
                            <h6 className="card-subtitle mb-2 text-muted text-center">
                                {model.manufacturer.name}
                            </h6>
                        </div>
                    </div>
                );
            })}
        </div>
    );
}
function ModelList() {
    const [modelColumns, setModelColumns] = useState([[], [], []]);

    async function getModels() {
        const url = 'http://localhost:8100/api/models/';

        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();

                const requests = [];
                for (let model of data.models) {
                    const detailUrl = `http://localhost:8100/${model.href}`
                    requests.push(fetch(detailUrl));
                }
                const responses = await Promise.all(requests);
                const modelColumns = [[], [], []];

                let i = 0;
                for (const modelResponse of responses) {
                    if (modelResponse.ok) {
                        const details = await modelResponse.json();
                        modelColumns[i].push(details);
                        i = i + 1;
                        if (i > 2) {
                            i = 0;
                        }
                    } else {
                        console.error(modelResponse);
                    }
                }
                setModelColumns(modelColumns);
            }
        }
        catch (e) {
            console.error(e);
        }
    }
    useEffect(() => {
        getModels();
    }, []);


    return (
        <>
            <div className="mb-3 mt-3 d-grid gap-2 d-md-flex justify-content-md-end">
                <Link to="/vehicles/create" className="btn btn-primary btn-lg px-4 gap-3">
                    Create Vehicle Model
                </Link>
            </div>
            <div className="px-4 py-5 my-5 mt-0 text-center">
                <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" width="600" />
                <h1 className="display-5 fw-bold">A wide array of models</h1>
                <div className="col-lg-6 mx-auto">
                    <p className="lead mb-4">
                        You want it, we got it.
                    </p>
                    <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                        {/* add button for adding new model */}
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="row">
                    {modelColumns.map((modelList, index) => {
                        return (
                            <ModelColumn key={index} list={modelList} />
                        );
                    })}
                </div>
            </div>
        </>



    )
}

export default ModelList;

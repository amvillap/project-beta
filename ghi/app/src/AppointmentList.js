import { useEffect, useState } from 'react';
function FindVIP(vinNo) {
    const [AutoInventory, setAutoInventory] = useState([]);
    const [vins, setVins] = useState([]);
    const getAutoInventory = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/')
        if (response.ok) {
            const data = await response.json();
            setAutoInventory(data.autos)
        }
        const vinArray = []
        for (const auto of AutoInventory) {
            vinArray.push(auto.vin)
        }
        setVins(vinArray);
    }
    useEffect(() => { getAutoInventory() }, []);
    let isVIP = "no"
    for (const vin of vins) {
        if (vinNo.vinNo === vin) {
            isVIP = "yes"
        }
    }
    return (
        <p>{isVIP}</p>
    )
}

function AppointmentList() {
    const [appointments, setAppointments] = useState([])
    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
            const noStatus = []
            const data = await response.json();
            const allAppointments = data.appointments
            for (const appointment of allAppointments) {
                if (appointment.status === 'select status') {
                    noStatus.push(appointment)
                }
            }
            setAppointments(noStatus);
        }
    }

    useEffect(() => {
        getData()
    }, []);
    const handleCancel = async (id) => {
        const url = `http://localhost:8080/api/appointments/${id}/cancel/`
        const fetchConfig = {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
            },
        };
        await fetch(url, fetchConfig);
        getData();
    }
    const handleFinish = async (id) => {
        const url = `http://localhost:8080/api/appointments/${id}/finish/`
        const fetchConfig = {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
            },
        };
        await fetch(url, fetchConfig);
        getData();
    }
    return (
        <div>
            <div className="container">
                <div className="row justify-content-between">
                    <div className="col-4">
                        <h1 className="display-5 fw-bold py-3">Appointments</h1>
                    </div>
                    <div className="col-4 align-self-center justify-content-end">
                        <a className="btn btn-outline-primary" href="http://localhost:3000/appointments/create" role="button"> Create Appointment </a>
                    </div>
                </div>
            </div>
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>VIN</th>
                    <th>VIP?</th>
                    <th>Customer</th>
                    <th>Date & Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                    <th>Status</th>

                </tr>
                </thead>
                <tbody>
                {appointments.map(appointment => {
                    return (
                    <tr key={appointment.id}>
                        <td>{ appointment.id }</td>
                        <td>{ appointment.vin }</td>
                        <td><FindVIP vinNo={appointment.vin}/></td>
                        <td>{ appointment.customer }</td>
                        <td>{ appointment.date_time }</td>
                        <td>{ appointment.technician.first_name }</td>
                        <td>{ appointment.reason }</td>
                        <td>
                            <div className="row">
                                <a className="btn btn-danger btn-sm my-2" to="#" role="button" onClick={() => handleCancel(appointment.id)}>cancel</a>
                                <a className="btn btn-success btn-sm" to="#" role="button" onClick={() => handleFinish(appointment.id)}>finish</a>
                            </div>
                        </td>
                    </tr>
                    );
                })}
                </tbody>
            </table>
        </div>
    );
}




export default AppointmentList;

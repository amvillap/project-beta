import { useEffect, useState } from 'react';
function FindVIP(vinNo) {
    const [AutoInventory, setAutoInventory] = useState([]);
    const [vins, setVins] = useState([]);
    const getAutoInventory = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/')
        if (response.ok) {
            const data = await response.json();
            setAutoInventory(data.autos)
        }
        const vinArray = []
        for (const auto of AutoInventory) {
            vinArray.push(auto.vin)
        }
        setVins(vinArray);
    }
    useEffect(() => { getAutoInventory() }, []);
    let isVIP = "no"
    for (const vin of vins) {
        if (vinNo.vinNo === vin) {
            isVIP = "yes"
        }
    }
    return (
        <p>{isVIP}</p>
    )
}
function ServiceHistory() {
    const [appointments, setAppointments] = useState([])
    const [query, setQuery] = useState("");
    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments)
        }
    }
    useEffect(() => {
        getData()
    }, []);
    return (
        <div>
            <div className="container">
                <div className="row justify-content-between py-3">
                    <div className="col-4">
                        <h1 className="display-5 fw-bold">Service History</h1>
                    </div>
                </div>
            </div>
            {/* SEARCH BAR */}
            <form>
                <div className="input-group mb-3">
                    <input
                        type="text"
                        className="form-control"
                        placeholder="Search by VIN #"
                        aria-describedby="button-addon2"
                        id="search"
                        name="vin"
                        onChange={(e) => setQuery(e.target.value)}
                    />
                </div>
            </form>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>VIN</th>
                        <th>VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.filter((appointment) => appointment.vin.includes(query)).map(appointment => {
                        return (
                            <tr key={appointment.id}>
                                <td>{appointment.id}</td>
                                <td>{appointment.vin}</td>
                                <td><FindVIP vinNo={appointment.vin} /></td>
                                <td>{appointment.customer}</td>
                                <td>{appointment.date_time}</td>
                                <td>{appointment.technician.first_name}</td>
                                <td>{appointment.reason}</td>
                                <td>{appointment.status} </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}
export default ServiceHistory;

import React, { useState, useEffect } from 'react';

function TechnicianForm() {
    const[formData, setFormData] = useState({
        employee_id:'',
        first_name:'',
        last_name:''
    })

    const[registered, setRegistered] = useState(false)


    const handleSubmit = async(event) => {
        event.preventDefault();
        const locationUrl= 'http://localhost:8080/api/technicians/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(locationUrl, fetchConfig);

        if(response.ok) {
            setFormData({
                employee_id:'',
                first_name:'',
                last_name:''
            })
            setRegistered(true);
            }
        }

        const handleChange = (e) => {
            const value=e.target.value;
            const inputName = e.target.name;
            setFormData({
                ...formData,
                [inputName]: value
            });
        }

    const formClasses = (!registered) ? '' : 'd-none';
    const messageClasses = (!registered) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';

    return (
        <div className="card shadow my-4">
            <div className="card-body">
                <form className={formClasses} onSubmit={handleSubmit} id="create-technician-form">
                    <h1 className="card-title">Add a Technician</h1>
                    <div className="row">
                        <div className="form-floating mb-3">
                            <input onChange={handleChange} required placeholder="employee_id" type="text" id="employee_id" name="employee_id" className="form-control" value={formData.employee_id} />
                            <label htmlFor="employee_id">Employee id</label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="form-floating mb-3">
                            <input onChange={handleChange} required placeholder="first_name" type="text" id="first_name" name="first_name" className="form-control" value={formData.first_name} />
                            <label htmlFor="first_name">First name</label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="form-floating mb-3">
                            <input onChange={handleChange} required placeholder="last_name" type="text" id="last_name" name="last_name" className="form-control" value={formData.last_name} />
                            <label htmlFor="last_name">Last name</label>
                        </div>
                    </div>
                    <div className="row">
                    <button className="btn btn-lg btn-primary">Add</button>
                    </div>
                </form>
                <div className={messageClasses} id="success-message">
                    Technician added
                </div>
            </div>
        </div>

    );
}


export default TechnicianForm;

import React, { useState, useEffect } from 'react';


function VehicleForm() {
    const [manufacturerId, setManufacturerId] = useState([])
    const [hasCreated, setHasCreated] = useState(false)
    const [formData, setFormData] = useState({
        name: '',
        picture_url: '',
    })

    const getData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setManufacturerId(data.manufacturers) // look at this one again
        }
    }

    useEffect(() => {
        getData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const modelUrl = 'http://localhost:8100/api/models/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-type': 'application/json',
            },
        };
        const response = await fetch(modelUrl, fetchConfig)

        if (response.ok) {
            setFormData({
                name: '',
                picture_url: '',
            })

            setHasCreated(true);
        };
    }

    const handleChangeName = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    const formClasses = (!hasCreated) ? '' : 'd-none';
    const messageClasses = (!hasCreated) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';

    return (
        <div className="my-5">
            <div className="row">
                <div className="col col-sm-auto">
                    <img
                        width="300"
                        className="bg-white rounded shadow d-block mx-auto mb-4"
                        src="/Automobile_stub.svg.png"
                        alt='Car logo'
                    />
                </div>
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">

                            <form className={formClasses} onSubmit={handleSubmit} id="create-model-form">
                                <h1 className="card-title">Add vehicle model</h1>
                                <p className="mb-3">
                                    Select manufacturer.
                                </p>

                                <div className="mb-3">
                                    <select onChange={handleChangeName} name="manufacturer_id" id="manufacturer_id" required>
                                        <option value="">manufacturer</option>
                                        {
                                            manufacturerId.map(manuid => {
                                                return (
                                                    <option key={manuid.id} value={manuid.id}>{manuid.name}</option>
                                                )
                                            })
                                        }
                                    </select>
                                </div>
                                <p className="mb-3">
                                    Input the model details.
                                </p>
                                <div className="row">
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleChangeName} required placeholder="Name" type="text" id="name" name="name" className="form-control" value={formData.name} />
                                            <label htmlFor="color">Name</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleChangeName} required placeholder="Picture URL" type="text" id="picture_url" name="picture_url" className="form-control" value={formData.picture_url} />
                                            <label htmlFor="style_name">Picture URL</label>
                                        </div>
                                    </div>
                                </div>
                                <button className="btn btn-lg btn-primary">Create</button>
                            </form>
                            <div className={messageClasses} id="success-message">
                                Created Vehicle Model!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );


}

export default VehicleForm

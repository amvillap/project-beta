import React, { useState, useEffect } from 'react';


function AutomobileForm() {
    const [models, setModels] = useState([])
    const [formData, setFormData] = useState({
        color: '',
        year: '',
        vin: '',
    })

    const [hasCreated, setHasCreated] = useState(false)

    const getData = async () => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setModels(data.models)
        }
    }

    useEffect(() => {
        getData();
    }, []);


    const handleSubmit = async (event) => {
        event.preventDefault();

        const auto_url = "http://localhost:8100/api/automobiles/";

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(auto_url, fetchConfig);

        if (response.ok) {
            setFormData({
                color: '',
                year: '',
                vin: '',
            })

            setHasCreated(true);
        };
    }

    const handleChangeName = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }


    const formClasses = (!hasCreated) ? '' : 'd-none';
    const messageClasses = (!hasCreated) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';

    return (
        <div className="my-5">
            <div className="row">
                <div className="col col-sm-auto">
                    <img
                        width="300"
                        className="bg-white rounded shadow d-block mx-auto mb-4"
                        src="/Automobile_stub.svg.png"
                        alt='Car logo'
                    />
                </div>

                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">

                            <form className={formClasses} onSubmit={handleSubmit} id="create-automobile-form">
                                <h1 className="card-title">Add an automobile to inventory</h1>
                                <p className="mb-3">
                                    Choose your model.
                                </p>

                                <div className="mb-3">
                                    <select onChange={handleChangeName} name="model_id" id="model_id" required>
                                        <option value="">Choose model</option>
                                        {
                                            models.map(model => {
                                                return (
                                                    <option key={model.id} value={model.id}>{model.name}</option>
                                                )
                                            })
                                        }
                                    </select>
                                </div>

                                <p className="mb-3">
                                    Input the car details.
                                </p>

                                <div className="row">
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleChangeName} required placeholder="Color" type="text" id="color" name="color" className="form-control" value={formData.color} />
                                            <label htmlFor="color">Color</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleChangeName} required placeholder="Year" type="number" id="year" name="year" className="form-control" value={formData.year} />
                                            <label htmlFor="style_name">Year</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleChangeName} required placeholder="Vehicle Registration Number" type="text" id="vin" name="vin" className="form-control" value={formData.vin} />
                                            <label htmlFor="fabric">VIN</label>
                                        </div>
                                    </div>
                                </div>
                                <button className="btn btn-lg btn-primary">Create</button>
                            </form>

                            <div className={messageClasses} id="success-message">
                                Created Automobile!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}


export default AutomobileForm

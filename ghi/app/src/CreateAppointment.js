import React, { useState, useEffect } from 'react';

function CreateAppointment() {
    const [technicians, setTechnicians] = useState([]);
    const [formData, setFormData] = useState({
        vin: '',
        reason: '',
        date_time: '',
        customer: '',
        technician_id: ''
    })

    const [hasCreated, setHasCreated] = useState(false)

    const getData = async () => {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    }
    useEffect(() => {
        getData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const locationUrl = "http://localhost:8080/api/appointments/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(locationUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                vin: '',
                reason: '',
                date_time: '',
                customer: '',
                technician_id: ''
            })
            setHasCreated(true);
        };
    }

    const handleChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }
    const formClasses = (!hasCreated) ? '' : 'd-none';
    const messageClasses = (!hasCreated) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';

    return (
        <div className="container p-3 d-flex justify-content-center">
            <div className="card shadow my-4 w-50 p-3 d-flex justify-content-center">
                <div className="card-body">
                    <form className={formClasses} onSubmit={handleSubmit} id="create-appointment-form">
                        <h1 className="card-title">Create an appointment</h1>
                        <div className="form-group">
                            <label htmlFor="customer">Customer name</label>
                            <input onChange={handleChange} type="text" name="customer" className="form-control" id="customer" placeholder='"John Smith"' value={formData.customer} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="vin">Vehicle VIN</label>
                            <input onChange={handleChange} type="text" name="vin" className="form-control" id="vin" placeholder="17-digit VIN" value={formData.vin} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="date_time">When</label>
                            <input onChange={handleChange} type="text" name="date_time" className="form-control" id="date_time" placeholder="YYYY-MM-DD HH:MM" value={formData.date_time} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="technician">Technicians</label>
                            <select onChange={handleChange} className="form-control" id="technician_id" name="technician_id" value={formData.technician_id} >
                                {technicians.map(technician => {
                                    return (
                                        <option key={technician.id} value={technician.id}>
                                            {technician.first_name} {technician.last_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="form-group">
                            <label htmlFor="reason">Reason for visit</label>
                            <textarea onChange={handleChange} name="reason" className="form-control" id="reason" rows="2" value={formData.reason}></textarea>
                        </div>
                        <button className="btn btn-lg btn-primary">Create appointment</button>
                    </form>
                    <div className={messageClasses} id="success-message">
                        Appointment created!
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CreateAppointment;

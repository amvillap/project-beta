import React, { useState, useEffect } from 'react';

function TechnicianList() {
    const [technicians, setTechnicians] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/technicians/');

        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians)
        }
    }
    useEffect(() => {
        getData()
    }, [])

    return (
        <div>
            <div className="container">
                <div className="row justify-content-between">
                    <div className="col-4">
                        <h1 className="display-5 fw-bold">Technicians</h1>
                    </div>
                    <div className="col-4 align-self-center justify-content-end">
                        <a className="btn btn-outline-primary" href="http://localhost:3000/technicians/create" role="button"> + Technician </a>
                    </div>
                </div>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {technicians.map(technician => {
                        return (
                            <tr key={technician.id}>
                                <td>{technician.employee_id}</td>
                                <td>{technician.first_name}</td>
                                <td>{technician.last_name}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}


export default TechnicianList

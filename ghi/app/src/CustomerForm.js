import React, { useState } from 'react';


function CustomerForm() {
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        address: '',
        phone_number: '',
    })

    const [hasCreated, setHasCreated] = useState(false)

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = "http://localhost:8090/api/customers/";

        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                first_name: '',
                last_name: '',
                address: '',
                phone_number: '',
            })

            setHasCreated(true);
        };
    }

    const handleChangeName = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    const formClasses = (!hasCreated) ? '' : 'd-none';
    const messageClasses = (!hasCreated) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';


    return (
        <div className="my-5">
            <div className="row">
                <div className="col col-sm-auto">
                    <img
                        width="300"
                        className="bg-white rounded shadow d-block mx-auto mb-4"
                        src="/customer.jpg"
                        alt='Customer Logo'
                    />
                </div>
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                            <form className={formClasses} onSubmit={handleSubmit} id="create-customer-form">
                                <p className="mb-3">
                                    Input customer details.
                                </p>

                                <div className="row">
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleChangeName} required placeholder="First Name" type="text" id="first_name" name="first_name" className="form-control" value={formData.first_name} />
                                            <label htmlFor="color">First Name</label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleChangeName} required placeholder="Last Name" type="text" id="last_name" name="last_name" className="form-control" value={formData.last_name} />
                                            <label htmlFor="fabric">Last Name</label>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleChangeName} required placeholder="Address" type="text" id="address" name="address" className="form-control" value={formData.address} />
                                            <label htmlFor="fabric">Address</label>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="form-floating mb-3">
                                            <input onChange={handleChangeName} required placeholder="Phone Number" type="tel" id="typePhone" name="phone_number" className="form-control" value={formData.phone_number} />
                                            <label className="form-label" htmlFor="typePhone">Phone Number</label>
                                        </div>
                                    </div>

                                </div>
                                <button className="btn btn-lg btn-primary">Create</button>
                            </form>
                            <div className={messageClasses} id="success-message">
                                Created Customer!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default CustomerForm

from django.db import models
from django.urls import reverse

# Create your models here.
class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=100)

    def get_api_url(self):
        return reverse("api_show_technician", kwargs={"pk":self.pk})

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

STATUS_CHOICES = (
    ('select status', 'SELECT STATUS'),
    ('canceled', 'CANCELED'),
    ('finished', 'FINISHED'),
)

class Appointment(models.Model):
    vin = models.CharField(
        null=True,
        max_length = 17,
        unique= True,
        blank= True
    )
    date_time = models.DateTimeField()
    reason = models.TextField()
    status = models.CharField(
        max_length=15,
        choices=STATUS_CHOICES,
        default='select status'
        )
    customer = models.CharField(max_length=100)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
        null= True,
        blank = True
    )
    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"pk":self.id})

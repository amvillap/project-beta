from django.shortcuts import render
from .models import Appointment, Technician, AutomobileVO
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse



# Create your views here.
from common.json import ModelEncoder

# ENCODERS
class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id"
    ]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold"
    ]

class AppointmentEncoder(ModelEncoder):
    model= Appointment
    properties = [
        "vin",
        "id",
        "date_time",
        "reason",
        "status",
        "customer",
        "technician"
    ]
    encoders = {
        "technician": TechnicianEncoder()
    }
    # def get_extra_data(self, o) :
    #     return {"technician" : o.technician.first_name}

# TECHNICIANS
@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        print("TECHNICIANS:", technicians)
        print("TECHNICIANS TYPE:", type(technicians))
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder= TechnicianEncoder,
            safe= False,
        )

@require_http_methods(["GET", "DELETE"])
def api_show_technician(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder = TechnicianEncoder,
                safe = False
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message":"That technician does not exist."})
            response.status_code = 404
            return response
    else: #DELETE
        try:
            technician = Technician.objects.get(id=pk)
            technician.delete()
            return JsonResponse({"message":"Technician deleted."})
        except Technician.DoesNotExist:
            return JsonResponse({"message":"That technician does not exist."})

# APPOINTMENTS

@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        print("                APPOINTMENTS:                ", appointments)
        print("APPOINTMENTS TYPE:", type(appointments))
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
            safe= False
        )
    else: #POST
        try:
            content = json.loads(request.body)
            technician_id = content["technician_id"]
            technician = Technician.objects.get(id=technician_id)
            content["technician"] = technician
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe = False
            )
        except:
            response = JsonResponse(
                {"message":"Failed to make appointment."}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE", "GET"])
def api_show_appointment(request, pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder= AppointmentEncoder,
                safe= False
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    else: #DELETE
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.delete()
            return JsonResponse(
                {"message":"Appointment deleted"}
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})

@require_http_methods(["PUT"])
def api_cancel_appointment(request, pk):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=pk)
            setattr(appointment,"status","canceled")
            appointment.save()
            return JsonResponse(
                appointment,
                encoder = AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["PUT"])
def api_finish_appointment(request, pk):
    if request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=pk)
            setattr(appointment,"status","finished")
            appointment.save()
            return JsonResponse(
                appointment,
                encoder = AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response
